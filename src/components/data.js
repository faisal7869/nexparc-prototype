const data = [
  {
    garageName: 'Ovation',
    garageID: 0,
    address: '10 Ware Street, Wari, Dhaka',
    availTime: '04:00pm-06:00pm',
    availDays: 'Sunday, Tuesday',
    cosType: 'Daily',
    cost: '65'
  },
  {
    garageName: 'Melrose',
    garageID: 1,
    address: '10 Ware Street, Wari, Dhaka',
    availTime: '04:00pm-06:00pm',
    availDays: 'Sunday, Tuesday',
    cosType: 'Hourly',
    cost: '20'
  },
  {
    garageName: 'Mayflower',
    garageID: 2,
    address: '10 Ware Street, Wari, Dhaka',
    availTime: '04:00pm-06:00pm',
    availDays: 'Sunday, Tuesday',
    cosType: 'Hourly',
    cost: '40'
  },
  {
    garageName: 'Navana',
    garageID: 3,
    address: '10 Ware Street, Wari, Dhaka',
    availTime: '04:00pm-06:00pm',
    availDays: 'Sunday, Tuesday',
    cosType: 'Daily',
    cost: '80'
  },
  {
    garageName: 'BTI',
    garageID: 4,
    address: '10 Ware Street, Wari, Dhaka',
    availTime: '04:00pm-06:00pm',
    availDays: 'Sunday, Tuesday',
    cosType: 'Daily',
    cost: '70'
  },
];

export default data;
