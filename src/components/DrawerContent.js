import React, { Component } from 'react';
import {View, ScrollView, Text, Image} from 'react-native';

export default class DrawerContent extends Component {
render() {
return (
  <ScrollView style={{flex: 1, padding: 20, backgroundColor: '#212121'}}>
    <Image
        style={{resizeMode: 'cover'}}
        source={require('../images/logo.png')}
    />
    <View style={{ flex: 1, flexDirection: 'row', marginTop: 20}}>
      <Image
          style={{width: 20, height: 20, marginRight: 10}}
          source={require('../images/home.png')}
      />
        <Text style={{color: 'white'}}>Home</Text>
    </View>
  </ScrollView>
)}
}
