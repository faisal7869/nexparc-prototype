import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
import {DatePicker} from 'react-native-wheel-picker-android';
import moment from 'moment';
import HideableView from 'react-native-hideable-view';
import style from './style';
import {
  View,
  Text,
  Image,
  TextInput,
  Keyboard,
  TouchableOpacity,
  AsyncStorage,
} from 'react-native';

const ACCESS_TOKEN = 'access_token';

const resetAction = NavigationActions.reset({
index: 0,
actions: [
  NavigationActions.navigate({ routeName: 'Home', params: {title: 'Login'}})
]
});

export default class Home extends Component {

  static navigationOptions = ({ navigation }) => {
    const params = navigation.state.params || {};
    const navigate = navigation.navigate;
    return{
      headerLeft: (
        <TouchableOpacity
                style={{alignItems:'center', marginLeft: 10}}
                onPress={() => navigate('DrawerOpen')} >
              <Image
                source={require('../images/menu.png')}
              	style={{width: 20, height: 20}}
              />
        </TouchableOpacity>
      ),

      headerRight: <TouchableOpacity
                      style={style.headerButton}
                      onPress={() => handleButton()} >
                      <Text style={style.hButtonText}>{params.title}</Text>
                    </TouchableOpacity>
  }
}

  state = {
    parkLocation: null,
    starTime: 'Start Time',
    endTime: 'End Time',
    selection : null,
    visibility: false,
    isLoggedIn: true,
    accessToken: "",
  }

  onDateSelected(date){
    var input = moment(date).format('DD MMM, YYYY | hh:mm a');
    console.log('A date has been picked: ', input);

    if(this.state.selection===0){
      this.setState({
        starTime: ''+input
      })
    }

    else{
      this.setState({
        endTime: ''+input
      });
    }
  }

  toggle(key){
    if(key===0){
      this.setState({
        selection:0
      });
    }
    if(key===1){
      this.setState({
        selection:1
      });
    }
    this.setState({
            visibility: !this.state.visibility
        });
    }

  componentDidMount() {
    handleButton = this.onButtonPreesed;
    setTimeout(()=>{ //making this async to simulate an API call
      this.getToken()
    },1000)
  }

  componentWillMount() {

  }

  async getToken() {
    try {
      let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
      if(!accessToken) {
          this.setState({isLoggedIn: false});
          this.props.navigation.setParams({title: 'Login'});
      } else {
          this.setState({isLoggedIn: true});
          this.props.navigation.setParams({title: 'Logout'});
          this.setState({accessToken: accessToken});
          console.log(accessToken);
      }
    } catch(error) {
        console.log("Something went wrong");
    }
  }

  async deleteToken() {
    try {
        await AsyncStorage.removeItem(ACCESS_TOKEN)
        console.log('Token deleted!');
    } catch(error) {
        console.log("Here is the problem!");
    }
  }

  onButtonPreesed = () =>{
    const {isLoggedIn} = this.state;
    if(!isLoggedIn){
      this.props.navigation.navigate('Login', {booking: false});
    }
    else{
      this.deleteToken();
      this.setState({isLoggedIn: false});
      this.props.navigation.dispatch(resetAction);
    }
  }

  navNext(){
    const {parkLocation, starTime, endTime} = this.state;
    this.props.navigation.navigate('GarageList', {location: parkLocation});

    console.log('Location ', parkLocation);
    console.log('starTime ', starTime);
    console.log('endTime ', endTime);
  }

  render() {
    let now = new Date()
    return (
      <Image style={style.container} source={require('../images/background3.jpg')}>
        <View style={style.inputWraper}>
          <Image style={style.icon} source={require('../images/location-from.png')}/>
          <TextInput
              style={style.input}
              placeholder='Parking Location'
              placeholderTextColor='white'
              underlineColorAndroid='transparent'
              autoCapitalize={'none'}
              returnKeyType={'done'}
              autoCorrect={true}
              keyboardType= 'default'
              onChangeText={(parkLocation) => this.setState({parkLocation})}
          />
        </View>

        <TouchableOpacity style={style.inputWraper}
          onPress={() => this.toggle(0)}>
          <Image style={style.icon} source={require('../images/start-flag.png')} />
          <Text style={style.input}>{this.state.starTime}</Text>
        </TouchableOpacity>

        <TouchableOpacity style={style.inputWraper}
          onPress={() => this.toggle(1)}>
          <Image style={style.icon} source={require('../images/end-time.png')} />
          <Text style={style.input}>{this.state.endTime}</Text>
        </TouchableOpacity>

        <HideableView visible={this.state.visibility} removeWhenHidden={true}>
          <View style={style.dateContainer}>
             <DatePicker
               initDate={now.toISOString()}
               onDateSelected={(date)=>this.onDateSelected(date)}/>
             <TouchableOpacity style={style.okButton}
                onPress={() => this.toggle(null)}>
                <Text style={style.buttonText}>OK</Text>
             </TouchableOpacity>
          </View>
        </HideableView>

        <TouchableOpacity style={style.button}
          onPress={() => this.navNext()}>
          <Text style={style.buttonText}>Search</Text>
        </TouchableOpacity>

      </Image>
    );
  }
}
