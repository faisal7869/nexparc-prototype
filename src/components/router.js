import React from 'react';
import { StackNavigator, DrawerNavigator } from 'react-navigation';
import {TouchableOpacity, ScrollView, Text, Image, Linking} from 'react-native';
import style from './style';
import Home from './home';
import GarageList from './garageList';
import ShowDirection from './showDirection';
import ShowMap from './showMap';
import GarageDetail from './garageDetail';
import Booking from './booking';
import Login from './login';
import DrawerContent from './DrawerContent';
import {Icon} from "react-native-elements";

const linkPressed = () => {
    Linking.openURL('http://www.google.com').catch(err => console.error('An error occurred', err));
}

export const HomeStack = StackNavigator({
  Home: {
    screen: Home,
    navigationOptions: ({navigation}) => ({
      headerTitle: (
        <TouchableOpacity style={{marginLeft:5}} onPress={linkPressed}>
          <Image source={require('../images/logo.png')}/>
        </TouchableOpacity>
      ),
      headerMode: 'screen',
      headerStyle:{
        backgroundColor: '#212121', },
    })
  },

  Login: {
    screen: Login,
    navigationOptions: ({ navigation }) => ({
      header: null,
    })
  },

  GarageList: {
    screen: GarageList,
    navigationOptions: {
      title: 'Search List',
      headerTitleStyle: {color: 'white'},
      headerTintColor: 'white',
      headerMode: 'screen',
      headerStyle:{
        backgroundColor: '#212121',}
    }
  },

  ShowDirection: {
    screen: ShowDirection,
    navigationOptions: {
      title: 'Garage Route',
      headerTitleStyle: {color: 'white'},
      headerTintColor: 'white',
      headerMode: 'screen',
      headerStyle:{
        backgroundColor: '#212121',}
    }
  },

  ShowMap: {
    screen: ShowMap,
    navigationOptions: {
      title: 'Map',
      headerTitleStyle: {color: 'white'},
      headerTintColor: 'white',
      headerMode: 'screen',
      headerStyle:{
        backgroundColor: '#212121',}
    }
  },

  Booking: {
    screen: Booking,
    navigationOptions: {
      title: 'Booking',
      headerTitleStyle: {color: 'white'},
      headerTintColor: 'white',
      headerMode: 'screen',
      headerStyle:{
        backgroundColor: '#212121',}
    }
  },

  GarageDetail: {
    screen: GarageDetail,
    navigationOptions: {
      title: 'Detail',
      headerTitleStyle: {color: 'white'},
      headerTintColor: 'white',
      headerMode: 'screen',
      headerStyle:{
        backgroundColor: '#212121',}
    }
  },
});

export const Router = DrawerNavigator({
  Main: {
    screen: HomeStack,
    navigationOptions: {
        drawerLabel: 'Home',
        drawerIcon: ({tintColor}) => <Icon name='home' color={tintColor}/>,
    }
  },
  About: {
    screen: Login,
    navigationOptions: {
        drawerLabel: 'About',
        drawerIcon: ({tintColor}) => <Icon name='info' color={tintColor}/>,
    }
  }
},
  {
    initialRouteName: 'Main',
    drawerWidth: 240,

    contentOptions: {
      activeTintColor: 'white',
      activeBackgroundColor: 'rgba(0, 0, 0, 0.2 )',
      inactiveTintColor: 'rgba(255, 255, 255, 0.4 )',

       style: {
        backgroundColor: '#252525',
        flex: 1
      }
    }
  }
)
