import {StyleSheet} from 'react-native';
import Dimensions from 'Dimensions';

const style = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    resizeMode: 'cover',
    height: null,
    width: null
  },

  inputWraper: {
    flexDirection: 'row',
    alignItems: 'center',
    width: Dimensions.get('window').width - 40,
    height: 40,
    marginBottom: 10,
    borderRadius: 30,
    backgroundColor: 'rgba(0, 0, 0, 0.5 )',
  },

  input: {
    width: Dimensions.get('window').width - 70,
    paddingHorizontal: 10,
    color: 'white',
    alignItems: 'center'

  },

  button: {
    width: Dimensions.get('window').width - 40,
    height: 40,
    justifyContent: 'center',
    borderRadius: 30,
    backgroundColor: '#212121',
  },

  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontWeight: 'bold'
  },

  icon: {
    width: 20,
    height: 20,
    marginLeft: 20,
  },

  dateContainer: {
    width: Dimensions.get('window').width - 40,
    height: 190,
    margin: 10,
    alignItems: 'center',
    borderRadius: 30,
    backgroundColor: 'rgba(0, 0, 0, 0.5 )',
  },

  rowContainer:{
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginTop: 3,
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 3,
    borderRadius: 10,
    backgroundColor: 'rgba(0, 0, 0, 0.5 )'
  },

  row: {
    flex: 0.6,
    flexDirection: 'column',
    marginLeft: 7,
  },

  buttonContainer: {
    flex: 0.4,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    margin: 5
  },

  rowButton: {
    marginRight: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },

  rowText: {
    color: 'white',
    fontSize: 15,
    padding: 1
  },

  okButton: {
    width: Dimensions.get('window').width - 40,
    height: 40,
    justifyContent: 'center',
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    backgroundColor: 'rgba(0, 0, 0, 0.9 )',
  },

  mapContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    borderWidth: 1,
    borderColor: 'white'
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },

  footButton: {
    flexDirection: 'row',
    alignItems:'flex-end',
    marginTop: 50,
 },

 headerButton: {
   margin: 5,
   marginRight: 10,
   borderWidth: 2,
   borderRadius: 5,
   borderColor: '#3d3d3d',
   backgroundColor: '#212121',
 },

 hButtonText: {
   color: 'white',
   textAlign: 'center',
   padding: 8,
   paddingHorizontal: 20,
   fontWeight: 'bold',
 }

});

export default style;
