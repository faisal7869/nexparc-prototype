import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import {Router, HomeStack} from './router';

export default class App extends Component {
  render() {
    return <Router/>
  }
}

AppRegistry.registerComponent('npPrototype1', () => App);
