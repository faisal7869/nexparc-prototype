import React, { Component } from 'react';
import style from './style';
import {
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';

export default class GarageDetail extends Component {

	render() {
    const { navigate } = this.props.navigation;
    const {goBack} = this.props.navigation;
		var garage = this.props.navigation.state.params.garage;

		return (
			<View style={{margin: 5, borderWidth: 1, borderColor: 'transparent'}}>
				<Image source={require('../images/garage.jpg')}
						style={{width: null, height: 300}} />

				<View style={{alignItems: "center", marginTop: 10}}>
					<Text style={{fontSize: 22, fontWeight: "bold", marginBottom: 10}}>
						{garage.garageName}
					</Text>
					<Text>{garage.address}</Text>
					<Text>Available Time: {garage.availTime}</Text>
					<Text>Available Days: {garage.availDays}</Text>
          <Text style={{fontWeight: "bold"}}>{garage.cosType}: ${garage.cost}</Text>

          <View style={style.footButton}>
  					<TouchableOpacity style={{margin: 5, backgroundColor: '#212121', width: 170, padding: 15, borderRadius: 20}}
  							onPress={() => navigate('Booking', {garage: garage})}>
  						<Text style={{fontSize: 22, color: "white", textAlign: "center"}}>
  							Book
  						</Text>
  					</TouchableOpacity>

            <TouchableOpacity style={{margin: 5, backgroundColor: '#212121', width: 170, padding: 15, borderRadius: 20}}
  							onPress={() => goBack(null)}>
  						<Text style={{fontSize: 22, color: "white", textAlign: "center"}}>
  							OK
  						</Text>
  					</TouchableOpacity>
          </View>

				</View>
			</View>
		)
	}
}
