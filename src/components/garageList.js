import React, { Component } from 'react';
import style from './style';
import data from './data'
import DialogBox from 'react-native-dialogbox';
import {Alert, ListView, View, Image, Text, TouchableOpacity} from 'react-native';

const info = data;

export default class OwnerList extends Component {

  constructor(props){
    super(props);
    const { params } = this.props.navigation.state;
    console.log("Location: ", params.location);

    const ds = new ListView.DataSource({rowHasChanged: this._rowHasChanged});
    this.state = {
      pageSize: 10,
      dataSource: ds.cloneWithRows(info)
    }
  }

  handleOnPress = () => {
		// alert
		Alert.alert("Insufficient balance!", 'Do you want to recharge?',
    [
      {text: 'OK', onPress: () => console.log('OK Pressed')},
      {text: 'Cancel', onPress: () => console.log('Cancel Pressed')},
      ],
      { cancelable: false })
	}

  render() {
    return (
      <Image
        style={{
          resizeMode: 'cover',
          height: null,
          width: null,
        }}
        source={require('../images/background3.jpg')}
        blurRadius={5} >

      <ListView
          dataSource={this.state.dataSource}
          renderRow={this._renderRow.bind(this)}
          enableEmptySections={true}
          initialListSize={1}
          pageSize={this.state.pageSize} />
      </Image>
    );
  }
  _renderRow(row) {
    const { navigate } = this.props.navigation;
    var self = this;
        return (
          <View style={style.rowContainer}>
            <View style= {style.row}>
                <Text style={{fontSize: 20, fontWeight: 'bold', color: 'white'}}>
                    {row.garageName}
                </Text>
                <Text style={style.rowText}>
                    {row.address}
                </Text>
                <Text style={style.rowText}>
                    Available Time: {row.availTime}
                </Text>
                <Text style={style.rowText}>
                    Available Days: {row.availDays}
                </Text>
                <Text style={[style.rowText, {paddingBottom: 5, fontWeight: 'bold'}]}>
                    {row.cosType}: ${row.cost}
                </Text>
            </View>

            <View style={style.buttonContainer}>
                <TouchableOpacity
                  style={style.rowButton}
                  underlayColor={"#1BB759"}
                  onPress={() => navigate('ShowMap', {garName: row.garageName})}>
                    <Image style={{width: 30, height: 30}} source={require('../images/map.png')} />
                </TouchableOpacity>

                <TouchableOpacity
                  style={style.rowButton}
                  underlayColor={"#1BB759"}
                  onPress={() => navigate('ShowDirection')}>
                    <Image style={{width: 30, height: 30}} source={require('../images/direction.png')} />
                </TouchableOpacity>

                <TouchableOpacity
                  style={style.rowButton}
                  underlayColor={"#1BB759"}
                  onPress={this.handleOnPress}>
                    <Image style={{width: 30, height: 30}} source={require('../images/booking.png')} />
                </TouchableOpacity>

                <TouchableOpacity
                  style={style.rowButton}
                  underlayColor={"#1BB759"}
                  onPress={() => navigate('GarageDetail', {garage: row})}>
                    <Image style={{width: 30, height: 30}} source={require('../images/details.png')} />
                </TouchableOpacity>
              </View>
            </View>
        )
  }

  _rowHasChanged(r1, r2) {
        return r1 !== r2
  }

}
