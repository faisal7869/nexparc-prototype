import React, { Component } from 'react';
import MapView from 'react-native-maps';
import Polyline from '@mapbox/polyline';
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";
import style from './style';
import {
  Text,
  View,
  Dimensions,
  BackHandler
} from 'react-native';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.005;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default class ShowDirection extends Component {

  constructor(props) {
    super(props);

    this.state = {
      initialPosition: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      },
      markerPosition: {
        latitude: 0,
        longitude: 0
      },
      coords: []
    }
  }

  watchID: ?number = null;

  componentDidMount(){
    LocationServicesDialogBox.checkLocationServicesIsEnabled({
            message: "<h2>Use Location ?</h2>This app wants to change your device settings:<br/><br/>Use GPS, Wi-Fi, and cell network for location<br/><br/><a href='#'>Learn more</a>",
            ok: "YES",
            cancel: "NO",
            enableHighAccuracy: true, // true => GPS AND NETWORK PROVIDER, false => ONLY GPS PROVIDER
            showDialog: true // false => Opens the Location access page directly
        }).then(function(success) {
            // success => {alreadyEnabled: true, enabled: true, status: "enabled"}
                navigator.geolocation.getCurrentPosition((position) => {
                  console.log(position);
                  var lat = parseFloat(position.coords.latitude)
                  var long = parseFloat(position.coords.longitude)
                  var loc = ""+lat+","+long

                  var initialRegion = {
                    latitude: lat,
                    longitude: long,
                    latitudeDelta: LATITUDE_DELTA,
                    longitudeDelta: LONGITUDE_DELTA
                  }

                  this.setState({initialPosition: initialRegion})
                  this.setState({markerPosition: initialRegion})
                  this.getDirections(""+loc, "23.7525842,90.3776929")
                },
                (error) => alert(JSON.stringify(error)),
                {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000});
            }.bind(this)
        ).catch((error) => {
            console.log(error.message);
        });

    this.watchID = navigator.geolocation.watchPosition((position) => {
      console.log(position);
      var lat = parseFloat(position.coords.latitude)
      var long = parseFloat(position.coords.longitude)

      var nextRegion = {
        latitude: lat,
        longitude: long
      }

      this.setState({markerPosition: nextRegion})
    })
  }

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.backPressed);
}

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID);
    BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
  }

  backPressed = () => {
    this.props.navigation.goBack();
    return true;
}


  async getDirections(startLoc, destinationLoc){
    try{
      let resp = await fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${ startLoc }&destination=${ destinationLoc }`)
      let respJson = await resp.json();
      let points = Polyline.decode(respJson.routes[0].overview_polyline.points);
      let coords = points.map((point, index) => {
        return{
          latitude: point[0],
          longitude: point[1]
        }
      })
      this.setState({coords: coords})
        return coords
      }
      catch(error){
        alert(error)
        return error
      }
    }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={style.mapContainer}>
        <MapView style={style.map}
            region={this.state.initialPosition} >

            <MapView.Marker
              coordinate={this.state.markerPosition}
              title={'YOU'}
            />

            <MapView.Polyline
              coordinates={this.state.coords}
              strokeWidth={6}
              strokeColor="#7BB5FF"
            />

        </MapView>
      </View>
    );
  }
}
