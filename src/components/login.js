import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
import Toast from 'react-native-root-toast';
import style from './loginStyle';
import {
  View, Text, Image, TextInput, Keyboard,
  TouchableOpacity, AsyncStorage, Alert,
  ActivityIndicator } from 'react-native';

const ACCESS_TOKEN = 'access_token';

const resetAction = NavigationActions.reset({
index: 0,
actions: [
  NavigationActions.navigate({ routeName: 'Home', params: {title: 'Logout'}})
]
});

export default class Login extends Component {

  constructor(props){
    super(props);
  }

  state = {
    email: '',
    password: '',
    error: "",
    showProgress: false
  }

  storeToken(responseData){
    AsyncStorage.setItem(ACCESS_TOKEN, responseData, (err)=> {
      if(err){
        console.log("an error");
        throw err;
      }
      console.log("success");
    }).catch((err)=> {
        console.log("error is: " + err);
    });
  }

  _showToast(message){
    Toast.show(''+message, {
          duration: Toast.durations.SHORT,
          position: Toast.positions.BOTTOM,
          shadow: true,
          animation: true,
          hideOnPress: true,
          delay: 0,
    });
  }

  async onLoginPressed() {
    Keyboard.dismiss();
    if(this.state.email==='' || this.state.password===''){
      Alert.alert("Fields can't be empty!")
    }
    else{
      this.setState({showProgress: true})
      try {
        let response = await fetch('https://afternoon-beyond-22141.herokuapp.com/api/login', {
                                method: 'POST',
                                headers: {
                                  'Accept': 'application/json',
                                  'Content-Type': 'application/json',
                                },
                                body: JSON.stringify({
                                  session:{
                                    email: this.state.email,
                                    password: this.state.password,
                                  }
                                })
                              });
        let res = await response.text();
        if (response.status >= 200 && response.status < 300) {
            //Handle success
            let accessToken = res;
            console.log(accessToken);
            //On success we will store the access_token in the AsyncStorage
            this.storeToken(accessToken);
            if(this.props.navigation.state.params.booking){
                this.props.navigation.goBack()
                this.setState({showProgress: false})
            }
            else {
                this.props.navigation.dispatch(resetAction)
                this.setState({showProgress: false})
              }

            this._showToast('Login successful!')

        } else {
            //Handle error
            let error = res;
            throw error;
        }
      } catch(error) {
          this.setState({error: error});
          this._showToast('Invalid email or password!');
          this.setState({showProgress: false});
          console.log("error " + error);
      }
    }
  }

  render() {
    return (
      <Image style={style.container} source={require('../images/background3.jpg')} >
      <View style={{flex:0.4, justifyContent: 'flex-end', alignItems: 'center', marginBottom: 30}}>
        <Image
            style={{resizeMode: 'contain'}}
            source={require('../images/logo.png')}
        />
        </View>
        <View style={{flex:0.6}}>

        <View style={style.loginContainer}>

          <View style={style.inputWraper}>
            <Image style={style.icon} source={require('../images/user.png')} />
            <TextInput
                style={style.input}
                placeholder='Username'
                placeholderTextColor='white'
                underlineColorAndroid='transparent'
    					  autoCapitalize={'none'}
    					  returnKeyType={'next'}
    					  autoCorrect={false}
                keyboardType= 'email-address'
                onChangeText={(email) => this.setState({email})}
                onSubmitEditing={() => this.passwordInput.focus()}
            />
          </View>

          <View style={style.inputWraper}>
            <Image style={style.icon} source={require('../images/password.png')} />
            <TextInput
                style={style.input}
                placeholder='Password'
                placeholderTextColor='white'
                underlineColorAndroid='transparent'
    					  autoCapitalize={'none'}
    					  returnKeyType={'done'}
    					  autoCorrect={false}
                keyboardType= 'default'
                secureTextEntry= {true}
                ref={(input) => this.passwordInput = input}
                onChangeText={(password) => this.setState({password})}
            />
          </View>

          <TouchableOpacity
            style={style.button}
            onPress={this.onLoginPressed.bind(this)}>
            <Text style={style.buttonText}>LOGIN</Text>
          </TouchableOpacity>

          <ActivityIndicator
            style={{opacity: this.state.showProgress ? 1.0 : 0.0, margin: 5}}
            animating={true} size="large"/>

        </View>
        </View>
      </Image>
    );
  }
}
