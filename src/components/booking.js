import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
import style from './style';
import {
  View,
  Text,
  Image,
  Alert,
  TouchableOpacity,
  AsyncStorage
} from 'react-native';

const ACCESS_TOKEN = 'access_token';

const resetAction = NavigationActions.reset({
index: 0,
actions: [
  NavigationActions.navigate({ routeName: 'Home'})
]
});

export default class Booking extends Component {

  async getToken() {
    try {
      let accessToken = await AsyncStorage.getItem(ACCESS_TOKEN);
      if(!accessToken) {
        Alert.alert('You are not logged in!',
          'To book this garage, you need to login. Do you wish to login?',
          [
            {text: 'OK', onPress: () => this.props.navigation.navigate('Login', {booking: true})},
            {text: 'Cancel', onPress: () => console.log('Cancel Pressed')},
          ],
        { cancelable: false })
      } else {
        Alert.alert('Booking Successful!')
          this.props.navigation.dispatch(resetAction)
          console.log(accessToken);
      }
    } catch(error) {
        console.log("Something went wrong");
    }
  }

  render() {
    const {goBack} = this.props.navigation;
    var garage = this.props.navigation.state.params.garage;

		return (
			<View style={{borderWidth: 1, borderColor: 'transparent'}}>
				<Image source={require('../images/garage.jpg')}
						style={{width: null, height: 300, margin: 5}} />

				<View style={{alignItems: "center", marginTop: 10}}>
					<Text style={{fontSize: 22, fontWeight: "bold", marginBottom: 10}}>
						{garage.garageName}
					</Text>
					<Text>{garage.address}</Text>
					<Text>Available Time: {garage.availTime}</Text>
					<Text>Available Days: {garage.availDays}</Text>
          <Text style={{fontWeight: "bold"}}>{garage.cosType}: ${garage.cost}</Text>

					<TouchableOpacity style={{marginTop: 40, backgroundColor: '#212121', width: 200, padding: 15, borderRadius: 20}}
              underlayColor={'#263238'}
              onPress={() => this.getToken()}>
						<Text style={{fontSize: 22, color: "white", textAlign: "center"}}>
							Confirm
						</Text>
					</TouchableOpacity>
				</View>
			</View>
		)
  }
}
