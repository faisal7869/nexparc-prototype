import React, { Component } from 'react';
import MapView from 'react-native-maps';
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";
import style from './style';
import {
  AppRegistry,
  Text,
  View,
  Dimensions
} from 'react-native';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.005;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default class ShowMap extends Component {

  constructor(props) {
    super(props);
    const { params } = this.props.navigation.state;

    this.state = {
      initialPosition: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      },

      markers: [{
        key: 1,
        title: params.garName,
        coordinates: {
          latitude: 23.7486935,
          longitude: 90.3801017
        },
      }]
    }
  }

  watchID: ?number = null;

  componentDidMount(){
    LocationServicesDialogBox.checkLocationServicesIsEnabled({
            message: "<h2>Use Location ?</h2>This app wants to change your device settings:<br/><br/>Use GPS, Wi-Fi, and cell network for location<br/><br/><a href='#'>Learn more</a>",
            ok: "YES",
            cancel: "NO",
            enableHighAccuracy: true, // true => GPS AND NETWORK PROVIDER, false => ONLY GPS PROVIDER
            showDialog: true // false => Opens the Location access page directly
        }).then(function(success) {
            // success => {alreadyEnabled: true, enabled: true, status: "enabled"}
                navigator.geolocation.getCurrentPosition((position) => {
                  console.log(position);
                  var lat = parseFloat(position.coords.latitude)
                  var long = parseFloat(position.coords.longitude)

                  var initialRegion = {
                    latitude: lat,
                    longitude: long,
                    latitudeDelta: LATITUDE_DELTA,
                    longitudeDelta: LONGITUDE_DELTA
                  }

                  var curlocation = {
                    key: 0,
                    title: 'YOU',
                    coordinates: initialRegion
                  }

                  this.setState({initialPosition: initialRegion})
                  this.setState({markers: this.state.markers.concat([curlocation])})
                },
                (error) => alert(JSON.stringify(error)),
                {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000});
            }.bind(this)
        ).catch((error) => {
            console.log(error.message);
        });
  }

  render() {
    return (
      <View style={style.mapContainer}>
        <MapView style={style.map}
            region={this.state.initialPosition} >

            {this.state.markers.map(marker => (
              <MapView.Marker
                key={marker.key}
                coordinate={marker.coordinates}
                title={marker.title}
              />
            ))}
          </MapView>
      </View>
    );
  }
}
