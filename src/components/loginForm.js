import React, { Component } from 'react';
import style from './loginStyle';
import { View, Text, Image, TextInput, TouchableOpacity, AsyncStorage } from 'react-native';

const ACCESS_TOKEN = 'access_token';

export default class LoginForm extends Component {

  constructor(props){
    super(props);
    const { navigate } = this.props.navigation;
  }

  state = {
    email: "",
    password: "",
    error: "",
    showProgress: false,
  }

  storeToken(responseData){
    AsyncStorage.setItem(ACCESS_TOKEN, responseData, (err)=> {
      if(err){
        console.log("an error");
        throw err;
      }
      console.log("success");
    }).catch((err)=> {
        console.log("error is: " + err);
    });
  }

  redirect(routeName, accessToken){
    navigate(routeName);
  }

  async onLoginPressed() {
    this.setState({showProgress: true})
    try {
      let response = await fetch('https://afternoon-beyond-22141.herokuapp.com/api/login', {
                              method: 'POST',
                              headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json',
                              },
                              body: JSON.stringify({
                                session:{
                                  email: this.state.email,
                                  password: this.state.password,
                                }
                              })
                            });
      let res = await response.text();
      if (response.status >= 200 && response.status < 300) {
          //Handle success
          let accessToken = res;
          console.log(accessToken);
          //On success we will store the access_token in the AsyncStorage
          this.storeToken(accessToken);
          this.redirect('Home');
      } else {
          //Handle error
          let error = res;
          throw error;
      }
    } catch(error) {
        this.setState({error: error});
        console.log("error " + error);
        this.setState({showProgress: false});
    }
  }

  render() {
    return (
      <View style={style.loginContainer}>

        <View style={style.inputWraper}>
          <Image style={style.icon} source={require('../images/user.png')} />
          <TextInput
              style={style.input}
              placeholder='Username'
              placeholderTextColor='white'
              underlineColorAndroid='transparent'
  					  autoCapitalize={'none'}
  					  returnKeyType={'next'}
  					  autoCorrect={false}
              keyboardType= 'email-address'
              onChangeText={(email) => this.setState({email})}
              onSubmitEditing={() => this.passwordInput.focus()}
          />
        </View>

        <View style={style.inputWraper}>
          <Image style={style.icon} source={require('../images/password.png')} />
          <TextInput
              style={style.input}
              placeholder='Password'
              placeholderTextColor='white'
              underlineColorAndroid='transparent'
  					  autoCapitalize={'none'}
  					  returnKeyType={'go'}
  					  autoCorrect={false}
              keyboardType= 'default'
              secureTextEntry= {true}
              ref={(input) => this.passwordInput = input}
              onChangeText={(password) => this.setState({password})}
          />
        </View>

        <TouchableOpacity
          style={style.button}
          onPress={this.onLoginPressed.bind(this)}>
          <Text style={style.buttonText}>LOGIN</Text>
        </TouchableOpacity>

      </View>
    );
  }
}
