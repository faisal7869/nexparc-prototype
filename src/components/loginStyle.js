import {StyleSheet} from 'react-native';
import Dimensions from 'Dimensions';

var style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: null,
    width: null,
    resizeMode: 'cover'
  },

  loginContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },

  inputWraper: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: Dimensions.get('window').width - 40,
    height: 40,
    marginBottom: 10,
    borderRadius: 30,
    backgroundColor: 'rgba(0, 0, 0, 0.5 )',
  },

  input: {
    width: Dimensions.get('window').width - 70,
    paddingHorizontal: 5,
    color: 'white',
    alignItems: 'center'

  },

  button: {
    width: Dimensions.get('window').width - 40,
    height: 40,
    justifyContent: 'center',
    borderRadius: 30,
    backgroundColor: '#212121',
  },

  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontWeight: 'bold'
  },

  icon: {
    width: 25,
    height: 25,
    marginLeft: 20,
  },
  
  loader: {
    marginTop: 20
  }

});

export default style;
